"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var faker_1 = __importDefault(require("faker"));
var app = express_1.default();
var port = 3000;
app.get('/', function (req, res) {
    res.send({
        cardNO: faker_1.default.random.number({ min: 100000000000000, max: 999999999999999 })
    });
});
// start the express server
app.listen(port, function () {
    // tslint:disable-next-line:no-console
    console.log("server started at http://localhost:" + port);
});
