import express from 'express';
import faker from 'faker';

const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send({
    cardNO: faker.random.number({ min: 100000000000000, max: 999999999999999 })
  });
});

// start the express server
app.listen(port, () => {
  // tslint:disable-next-line:no-console
  console.log(`server started at http://localhost:${port}`);
});